#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]

use defmt::*;
use embassy_executor::Spawner;
use embassy_rp::i2c::{self, Config};
use embassy_rp::interrupt;
use embassy_time::{Duration, Timer};
use embedded_hal_async::i2c::I2c;
use {defmt_rtt as _, panic_probe as _};

#[allow(dead_code)]
mod mpu6050 {
    pub const ADDR: u8 = 0x68; // addr pin low (otherwise its 0x69 (nice))

    macro_rules! mpuregs {
        ($($name:ident : $val:expr),* $(,)?) => {
            $(
                pub const $name: u8 = $val;
            )*

            pub fn regname(reg: u8) -> &'static str {
                match reg {
                    $(
                        $val => stringify!($name),
                    )*
                    _ => panic!("bad reg"),
                }
            }
        }
    }

    // MPU6050 Register Addresses
    // Copied these definitions from the Arduino example code for the GY-86
    mpuregs! {
        XG_OFFS_TC: 0x00, //[7] PWR_MODE, [6:1] XG_OFFS_TC, [0] OTP_BNK_VLD
        YG_OFFS_TC: 0x01, //[7] PWR_MODE, [6:1] YG_OFFS_TC, [0] OTP_BNK_VLD
        ZG_OFFS_TC: 0x02, //[7] PWR_MODE, [6:1] ZG_OFFS_TC, [0] OTP_BNK_VLD
        X_FINE_GAIN: 0x03, //[7:0] X_FINE_GAIN
        Y_FINE_GAIN: 0x04, //[7:0] Y_FINE_GAIN
        Z_FINE_GAIN: 0x05, //[7:0] Z_FINE_GAIN
        XA_OFFS_H: 0x06, //[15:0] XA_OFFS
        XA_OFFS_L_TC: 0x07,
        YA_OFFS_H: 0x08, //[15:0] YA_OFFS
        YA_OFFS_L_TC: 0x09,
        ZA_OFFS_H: 0x0A, //[15:0] ZA_OFFS
        ZA_OFFS_L_TC: 0x0B,
        XG_OFFS_USRH: 0x13, //[15:0] XG_OFFS_USR
        XG_OFFS_USRL: 0x14,
        YG_OFFS_USRH: 0x15, //[15:0] YG_OFFS_USR
        YG_OFFS_USRL: 0x16,
        ZG_OFFS_USRH: 0x17, //[15:0] ZG_OFFS_USR
        ZG_OFFS_USRL: 0x18,
        SMPLRT_DIV: 0x19,
        CONFIG: 0x1A,
        GYRO_CONFIG: 0x1B,
        ACCEL_CONFIG: 0x1C,
        FF_THR: 0x1D,
        FF_DUR: 0x1E,
        MOT_THR: 0x1F,
        MOT_DUR: 0x20,
        ZRMOT_THR: 0x21,
        ZRMOT_DUR: 0x22,
        FIFO_EN: 0x23,
        I2C_MST_CTRL: 0x24,
        I2C_SLV0_ADDR: 0x25,
        I2C_SLV0_REG: 0x26,
        I2C_SLV0_CTRL: 0x27,
        I2C_SLV1_ADDR: 0x28,
        I2C_SLV1_REG: 0x29,
        I2C_SLV1_CTRL: 0x2A,
        I2C_SLV2_ADDR: 0x2B,
        I2C_SLV2_REG: 0x2C,
        I2C_SLV2_CTRL: 0x2D,
        I2C_SLV3_ADDR: 0x2E,
        I2C_SLV3_REG: 0x2F,
        I2C_SLV3_CTRL: 0x30,
        I2C_SLV4_ADDR: 0x31,
        I2C_SLV4_REG: 0x32,
        I2C_SLV4_DO: 0x33,
        I2C_SLV4_CTRL: 0x34,
        I2C_SLV4_DI: 0x35,
        I2C_MST_STATUS: 0x36,
        INT_PIN_CFG: 0x37,
        INT_ENABLE: 0x38,
        DMP_INT_STATUS: 0x39,
        INT_STATUS: 0x3A,
        ACCEL_XOUT_H: 0x3B,
        ACCEL_XOUT_L: 0x3C,
        ACCEL_YOUT_H: 0x3D,
        ACCEL_YOUT_L: 0x3E,
        ACCEL_ZOUT_H: 0x3F,
        ACCEL_ZOUT_L: 0x40,
        TEMP_OUT_H: 0x41,
        TEMP_OUT_L: 0x42,
        GYRO_XOUT_H: 0x43,
        GYRO_XOUT_L: 0x44,
        GYRO_YOUT_H: 0x45,
        GYRO_YOUT_L: 0x46,
        GYRO_ZOUT_H: 0x47,
        GYRO_ZOUT_L: 0x48,
        EXT_SENS_DATA_00: 0x49,
        EXT_SENS_DATA_01: 0x4A,
        EXT_SENS_DATA_02: 0x4B,
        EXT_SENS_DATA_03: 0x4C,
        EXT_SENS_DATA_04: 0x4D,
        EXT_SENS_DATA_05: 0x4E,
        EXT_SENS_DATA_06: 0x4F,
        EXT_SENS_DATA_07: 0x50,
        EXT_SENS_DATA_08: 0x51,
        EXT_SENS_DATA_09: 0x52,
        EXT_SENS_DATA_10: 0x53,
        EXT_SENS_DATA_11: 0x54,
        EXT_SENS_DATA_12: 0x55,
        EXT_SENS_DATA_13: 0x56,
        EXT_SENS_DATA_14: 0x57,
        EXT_SENS_DATA_15: 0x58,
        EXT_SENS_DATA_16: 0x59,
        EXT_SENS_DATA_17: 0x5A,
        EXT_SENS_DATA_18: 0x5B,
        EXT_SENS_DATA_19: 0x5C,
        EXT_SENS_DATA_20: 0x5D,
        EXT_SENS_DATA_21: 0x5E,
        EXT_SENS_DATA_22: 0x5F,
        EXT_SENS_DATA_23: 0x60,
        MOT_DETECT_STATUS: 0x61,
        I2C_SLV0_DO: 0x63,
        I2C_SLV1_DO: 0x64,
        I2C_SLV2_DO: 0x65,
        I2C_SLV3_DO: 0x66,
        I2C_MST_DELAY_CTRL: 0x67,
        SIGNAL_PATH_RESET: 0x68,
        MOT_DETECT_CTRL: 0x69,
        USER_CTRL: 0x6A,
        PWR_MGMT_1: 0x6B,
        PWR_MGMT_2: 0x6C,
        BANK_SEL: 0x6D,
        MEM_START_ADDR: 0x6E,
        MEM_R_W: 0x6F,
        DMP_CFG_1: 0x70,
        DMP_CFG_2: 0x71,
        FIFO_COUNTH: 0x72,
        FIFO_COUNTL: 0x73,
        FIFO_R_W: 0x74,
        WHO_AM_I: 0x75,
    }

    // HMC5883L Register Addresses
    //#define hmc5883l_ra_config_a       0x00
    //#define hmc5883l_ra_config_b       0x01
    //#define hmc5883l_ra_mode           0x02
    //#define hmc5883l_ra_data_out_x_h   0x03
    //#define hmc5883l_ra_data_out_x_l   0x04
    //#define hmc5883l_ra_data_out_z_h   0x05
    //#define hmc5883l_ra_data_out_z_l   0x06
    //#define hmc5883l_ra_data_out_y_h   0x07
    //#define hmc5883l_ra_data_out_y_l   0x08
    //#define hmc5883l_ra_status         0x09
    //#define hmc5883l_ra_id_a           0x0A
    //#define hmc5883l_ra_id_b           0x0B
    //#define hmc5883l_ra_id_c           0x0C

    //#define mpu6050_i2c_address mpu6050_DEFAULT_ADDRESS
    //#define hmc5883l_i2c_address 0x1e //datasheet said 0x3C but this is the already shifted value => 0x3C/2 = 0x1e
    //#define GYROSCOPE_DLPF_SET false
    //#define SAMPLE_RATE_GYROSCOPE_MAX 8000
    //#define SAMPLE_RATE_ACCELEROMETER_MAX 1000
    //#define SAMPLE_RATE_MAGNETOMETER_MAX 75
    //#if GYROSCOPE_DLPF_SET
    //#define SMPLRT_DIV_BASE 1
    //#else
    //#define SMPLRT_DIV_BASE 80
    //#endif
    //#define SMPLRT_DIV_MPU6050 (SMPLRT_DIV_BASE - 1) //CAUTION! Has to be at least to be SAMPLE_RATE_GYROSCOPE_MAX / SAMPLE_RATE_ACCELEROMETER_MAX - 1
    //#define SMPLRT_DIV_SLAVE0_MST_DLY  ((uint8_t)((SAMPLE_RATE_GYROSCOPE_MAX / (SAMPLE_RATE_MAGNETOMETER_MAX * (SMPLRT_DIV_MPU6050 + 1)))))

    //#define SAMPLE_RATE_GYROSCOPE (((double)SAMPLE_RATE_GYROSCOPE_MAX) / (SMPLRT_DIV_MPU6050 +1))
    //#define SAMPLE_RATE_ACCELEROMETER SAMPLE_RATE_GYROSCOPE //assuming SMPLRT_DIV_MPU6050 is at least  SAMPLE_RATE_GYROSCOPE_MAX / SAMPLE_RATE_ACCELEROMETER_MAX
    //#define SAMPLE_RATE_MAGNETOMETER (((double)SAMPLE_RATE_GYROSCOPE) / (SMPLRT_DIV_SLAVE0_MST_DLY * SMPLRT_DIV_MPU6050))

    //#define TARGET_SAMPLE_RATE_CURRENT_SENSOR SAMPLE_RATE_GYROSCOPE
    //#define WAIT_TICKS_CURRENT_SENSOR (pdMS_TO_TICKS(1000.0/TARGET_SAMPLE_RATE_CURRENT_SENSOR) + 1)
    //#define SAMPLE_RATE_CURRENT_SENSOR (1000.0 / (WAIT_TICKS_CURRENT_SENSOR*portTICK_PERIOD_MS))

    //#define FIFO_BUFFER_LENGTH 56
}

#[embassy_executor::main]
async fn main(_spawner: Spawner) {
    let p = embassy_rp::init(Default::default());

    let sda = p.PIN_14;
    let scl = p.PIN_15;
    let irq = interrupt::take!(I2C1_IRQ);

    info!("set up i2c ");
    let mut i2c = i2c::I2c::new_async(p.I2C1, scl, sda, irq, Config::default());

    use mpu6050::*;

    loop {
        let mut whoami = [0];
        match i2c
            .write_read(mpu6050::ADDR, &[mpu6050::WHO_AM_I], &mut whoami)
            .await
        {
            Ok(()) => info!("Found device at address and it replied with {}", whoami[0]),
            Err(e) => info!("Could not read address from device: {}", e),
        }
        Timer::after(Duration::from_secs(5)).await;
    }
}
